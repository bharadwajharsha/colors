package com.example.webview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity {

   private EditText field;
   private WebView browser;

   @Override		
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      //field = (EditText)findViewById(R.id.urlField);
      browser = (WebView)findViewById(R.id.webView1);
      browser.setWebViewClient(new MyBrowser());
      browser.addJavascriptInterface(new WebAppInterface(this), "Android");
      ActionBar bar = getActionBar();
      bar.setDisplayHomeAsUpEnabled(true);
      this.open(browser);
   }


   public void open(View view){
     // String url = "file:///android_asset/home.html";//field.getText().toString();
      browser.getSettings().setLoadsImagesAutomatically(true);
      browser.getSettings().setJavaScriptEnabled(true);
      browser.getSettings().setAllowFileAccess(true);
      //browser.getSettings().setAllowFileAccessFromFileURLs(true);
      browser.getSettings().setSupportZoom(false);
      browser.setLongClickable(false);

      browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
      browser.loadUrl("file:///android_asset/game1.html");
   }
   private class MyBrowser extends WebViewClient {
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
    	  Log.d("WEBVIEW","shouldOverrideUrlLoading Called ");
         view.loadUrl(url);
         return true;
      }
   }

   @Override
   public boolean onTouchEvent(MotionEvent event) {

       // do your stuff here... the below call will make sure the touch also goes to the webview.
	   Log.d("WEBVIEW","onTouchEvent Called "+event.getAction());
       return super.onTouchEvent(event);
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         // Respond to the action bar's Up/Home button
         case android.R.id.home:
         NavUtils.navigateUpFromSameTask(this);
         return true;
      }
      return super.onOptionsItemSelected(item);
   }

}
